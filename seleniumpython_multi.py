#!/usr/bin/python
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import os, re
import datetime
from urllib import request
from PyPDF2 import PdfFileMerger

# map Keys
list_news = {'LM': 'lemonde', 'LF': 'figaro', 'LI': 'liberation', 'EC': 'lesechos', 'EQ': 'lequipe', 'EX': 'lexpress'}

print(list_news)

for news in list_news:
    #news = input("Enter acronym: ")
    try:
        print(f'{news} for {list_news[news]} selected')
    except KeyError:
        print(f"{news} not supported. Exiting...")
        raise SystemExit

    # today
    d = datetime.date.today()
    #d = d - datetime.timedelta(days=1) # comment line for today
    if news == 'EC':
        # previous day for les echos
        d = d - datetime.timedelta(days=1)
        print(f"{news} for day "+d.strftime('%Y%m%d'))
    elif news == 'LM':
        # next day for le monde
        d = d - datetime.timedelta(days=-1)
        print(f"{news} for day "+d.strftime('%Y%m%d'))

    today=d.strftime('%Y%m%d')

    # define the name of the directory to be created
    basepath = "/PATH_TO/journaux/"+list_news[news]+"/"
    path = basepath+str(d)


    try:
        os.mkdir(path)
    except OSError:
        print ("Creation of the directory %s failed" % path)
    else:
        print ("Successfully created the directory %s " % path)

    # For Chrome

    chrome_profile = webdriver.ChromeOptions()
    prefs = {"download.default_directory": path,
            "download.prompt_for_download": False,
            "download.directory_upgrade": True,
            "plugins.always_open_pdf_externally": True}
    chrome_profile.add_experimental_option("prefs",prefs)
    browser = webdriver.Chrome(options=chrome_profile)

    # For Firefox
    #fp = webdriver.FirefoxProfile()
    #fp.set_preference("browser.download.folderList", 2)
    #fp.set_preference("browser.download.manager.showWhenStarting", False)
    #fp.set_preference("browser.download.dir", path)
    #fp.set_preference('browser.helperApps.alwaysAsk.force', False)
    #fp.set_preference("plugin.disable_full_page_plugin_for_types", "application/pdf")
    #fp.set_preference("pdfjs.disabled", True)
    #fp.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf")
    #browser = webdriver.Firefox(fp)

    browser.get('https://sid2nomade-1.grenet.fr/login?url=http://nouveau.europresse.com/access/ip/default.aspx?un=grenobleT_1')

    elem = browser.find_element_by_name('user')
    elem.send_keys('USERNAME')
    elem = browser.find_element_by_name('pass')
    elem.send_keys('PASSWD' + Keys.RETURN)
    time.sleep(5)
    elem = browser.find_element_by_id("Keywords")
    elem.click()
    elem.send_keys(list_news[news] + Keys.RETURN)
    print("starting download...")
    time.sleep(8)


    n = 38 # en general il n'y a pas plus de 35 pages pour les quotidiens
    if news == 'EX':
        # 80 pages de l'express
        n = 83

    for i in range(1, n):
        print("page "+str(i))
        browser.get("https://nouveau-europresse-com.sid2nomade-1.grenet.fr/WebPages/Pdf/Document.aspx?DocName=pdf%C2%B7"+today+"%C2%B7"+news+"_p%C2%B7"+str(i))
        print("https://nouveau-europresse-com.sid2nomade-1.grenet.fr/WebPages/Pdf/Document.aspx?DocName=pdf%C2%B7"+today+"%C2%B7"+news+"_p%C2%B7"+str(i))
        time.sleep(3)

    print("end...")
    time.sleep(3)

    browser.quit()

    # merge pdfs

    print("merging pdfs...")

    try:
        os.chdir(path)
    except OSError:
        print ("Change directory %s failed" % path)
    else:
        print ("Successfully changed directory %s " % path)

    x = [a for a in os.listdir() if a.endswith(".pdf")]
    def sorted_alphanumeric(data): # tried different sorts : winner
        convert = lambda text: int(text) if text.isdigit() else text.lower()
        alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
        return sorted(data, key=alphanum_key)

    pdflist = sorted_alphanumeric(x)
    print(pdflist)
    if pdflist:
        merger = PdfFileMerger()

        for pdf in pdflist:
            merger.append(open(pdf, 'rb'))

        with open(news+"-"+str(d)+".pdf", "wb") as fout:
            merger.write(fout)

        print("output pdf is "+news+"-"+str(d)+".pdf in "+path)

        print("removing single page pdfs...")

        for name in pdflist:
            try:
                os.remove(os.path.join(path, name))
            except OSError:
                print ("deletion of %s failed" % name)
            else:
                print ("Successfully deleted %s " % name)

        print("done "+list_news[news]+".")
    else:
        print("nothing downloaded for "+list_news[news]+" :(")
print("end_")
